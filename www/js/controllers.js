angular.module('starter.controllers', [])

.controller('HomeCtrl', function($scope, $state) {
    $scope.jogo = function(){
        $state.go("jogo")
    };
})

.controller('JogoCtrl', function($scope, $state) {
    $scope.home = function(){
        $state.go("home")
    };
});
